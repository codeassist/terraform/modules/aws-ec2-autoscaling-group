locals {
  # To avoid error `join: only works on flat lists` split complex variable into flatten lists
  iam_instance_profiles_list = coalescelist(
    aws_launch_template.this.*.iam_instance_profile,
    [
      [
        {
          name = ""
        },
      ],
    ],
  )
  iam_instance_profiles_entities = local.iam_instance_profiles_list[0]
  iam_instance_profile_map       = local.iam_instance_profiles_entities[0]
  iam_instance_profile           = local.iam_instance_profile_map["name"]
}

output "launch_template_id" {
  description = "The ID of the launch template."
  value       = join("", aws_launch_template.this.*.id)
}

output "launch_template_arn" {
  description = "The ARN of the launch template."
  value       = join("", aws_launch_template.this.*.arn)
}

output "launch_template_iam_instance_profile" {
  description = "The IAM instance profile name to associate with launched instances."
  value       = local.iam_instance_profile
}

output "autoscaling_group_id" {
  description = "The autoscaling group id."
  value       = join("", aws_autoscaling_group.this.*.id)
}

output "autoscaling_group_name" {
  description = "The autoscaling group name."
  value       = join("", aws_autoscaling_group.this.*.name)
}

output "autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group."
  value       = join("", aws_autoscaling_group.this.*.arn)
}

output "autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group."
  value       = join("", aws_autoscaling_group.this.*.min_size)
}

output "autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group."
  value       = join("", aws_autoscaling_group.this.*.max_size)
}

output "autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group."
  value       = join("", aws_autoscaling_group.this.*.desired_capacity)
}

output "autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity."
  value       = join("", aws_autoscaling_group.this.*.default_cooldown)
}

output "autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health."
  value       = join("", aws_autoscaling_group.this.*.health_check_grace_period)
}

output "autoscaling_group_health_check_type" {
  description = "`EC2` or `ELB`. Controls how health checking is done."
  value       = join("", aws_autoscaling_group.this.*.health_check_type)
}
