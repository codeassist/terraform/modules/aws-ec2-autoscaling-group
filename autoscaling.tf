# --------------------------
# AutoScaling configuration
# --------------------------

# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ec2-metricscollected.html

resource "aws_autoscaling_policy" "scale_up" {
  # Provides an AutoScaling Scaling Policy resource.
  count = local.autoscaling_enabled ? 1 : 0

  # (Required) The name of the policy.
  name = format("%s-scale-up-policy", local.asg_name_prefix_wo_dash)
  # (Required) The name of the autoscaling group.
  autoscaling_group_name = join("", aws_autoscaling_group.this.*.name)
  # (Optional) Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid
  # values are
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity.
  adjustment_type = var.ec2_asg_scale_up_adjustment_type
  # (Optional) The policy type, either:
  #   * SimpleScaling
  #   * StepScaling
  #   * TargetTrackingScaling
  policy_type = var.ec2_asg_scale_up_policy_type

  # ----------------------------------------------------------------------------
  # The following arguments are only available to "SimpleScaling" type policies:
  ##
  # (Optional) The amount of time, in seconds, after a scaling activity completes and before the next scaling
  # activity can start.
  cooldown = var.ec2_asg_scale_up_cooldown_seconds
  # (Optional) The number of instances by which to scale. adjustment_type determines the interpretation of this number
  # (e.g., as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
  # adds to the current capacity and a negative value removes from the current capacity.
  scaling_adjustment = var.ec2_asg_scale_up_scaling_adjustment
}

resource "aws_autoscaling_policy" "scale_down" {
  # Provides an AutoScaling Scaling Policy resource.
  count = local.autoscaling_enabled ? 1 : 0

  # (Required) The name of the policy.
  name = format("%s-scale-down-policy", local.asg_name_prefix_wo_dash)
  # (Required) The name of the autoscaling group.
  autoscaling_group_name = join("", aws_autoscaling_group.this.*.name)
  # (Optional) Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid
  # values are
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity.
  adjustment_type = var.ec2_asg_scale_down_adjustment_type
  # (Optional) The policy type, either:
  #   * SimpleScaling
  #   * StepScaling
  #   * TargetTrackingScaling
  policy_type = var.ec2_asg_scale_down_policy_type

  # ----------------------------------------------------------------------------
  # The following arguments are only available to "SimpleScaling" type policies:
  ##
  # (Optional) The amount of time, in seconds, after a scaling activity completes and before the next scaling
  # activity can start.
  cooldown = var.ec2_asg_scale_down_cooldown_seconds
  # (Optional) The number of instances by which to scale. adjustment_type determines the interpretation of this number
  # (e.g., as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
  # adds to the current capacity and a negative value removes from the current capacity.
  scaling_adjustment = var.ec2_asg_scale_down_scaling_adjustment
}

### HIGH ###
resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  # Provides a CloudWatch Metric Alarm resource.
  count = local.autoscaling_enabled ? 1 : 0

  # (Required) The descriptive name for the alarm. This name must be unique within the user's AWS account.
  alarm_name = format("%s-cpu-high-utilization-alarm", local.asg_name_prefix_wo_dash)
  # (Required) The arithmetic operation to use when comparing the specified Statistic and Threshold. The specified
  # Statistic value is used as the first operand. Either of the following is supported:
  #   * GreaterThanOrEqualToThreshold
  #   * GreaterThanThreshold
  #   * LessThanThreshold
  #   * LessThanOrEqualToThreshold
  comparison_operator = "GreaterThanOrEqualToThreshold"
  # (Required) The number of periods over which data is compared to the specified threshold.
  evaluation_periods = var.ec2_asg_cpu_utilization_high_evaluation_periods
  # (Optional) The name for the alarm's associated metric. See docs for supported metrics at:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  metric_name = "CPUUtilization"
  # The namespace for the alarm's associated metric. See docs for the list of namespaces at:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/aws-namespaces.html
  namespace = "AWS/EC2"
  # (Optional) The period in seconds over which the specified statistic is applied.
  period = var.ec2_asg_cpu_utilization_high_period_seconds
  # (Optional) The statistic to apply to the alarm's associated metric. Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  statistic = var.ec2_asg_cpu_utilization_high_statistic
  # (Required) The value against which the specified statistic is compared.
  threshold = var.ec2_asg_cpu_utilization_high_threshold_percent
  # (Optional) The list of actions to execute when this alarm transitions into an ALARM state from any other state.
  # Each action is specified as an Amazon Resource Name (ARN).
  alarm_actions = [join("", aws_autoscaling_policy.scale_up.*.arn)]
  # (Optional) The description for the alarm.
  alarm_description = "Scale AutoScaling [${local.asg_name_prefix_wo_dash}] Group up if CPU utilization is above [${var.ec2_asg_cpu_utilization_high_threshold_percent}] for [${var.ec2_asg_cpu_utilization_high_period_seconds}] seconds."
  # (Optional) The dimensions for the alarm's associated metric. For the list of available dimensions see
  # the AWS documentation:
  #   * http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  dimensions = {
    AutoScalingGroupName = join("", aws_autoscaling_group.this.*.name)
  }
}

### LOW ###
resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  # Provides a CloudWatch Metric Alarm resource.
  count = local.autoscaling_enabled ? 1 : 0

  # (Required) The descriptive name for the alarm. This name must be unique within the user's AWS account
  alarm_name = format("%s-cpu-low-utilization-alarm", local.asg_name_prefix_wo_dash)
  # (Required) The arithmetic operation to use when comparing the specified Statistic and Threshold. The specified
  # Statistic value is used as the first operand. Either of the following is supported:
  #   * GreaterThanOrEqualToThreshold,
  #   * GreaterThanThreshold,
  #   * LessThanThreshold,
  #   * LessThanOrEqualToThreshold.
  comparison_operator = "LessThanOrEqualToThreshold"
  # (Required) The number of periods over which data is compared to the specified threshold.
  evaluation_periods = var.ec2_asg_cpu_utilization_low_evaluation_periods
  # (Optional) The name for the alarm's associated metric. See docs for supported metrics at:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  metric_name = "CPUUtilization"
  # (Optional) The namespace for the alarm's associated metric. See docs for the list of namespaces at:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/aws-namespaces.html
  namespace = "AWS/EC2"
  # (Optional) The period in seconds over which the specified statistic is applied.
  period = var.ec2_asg_cpu_utilization_low_period_seconds
  # (Optional) The statistic to apply to the alarm's associated metric. Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  statistic = var.ec2_asg_cpu_utilization_low_statistic
  # (Required) The value against which the specified statistic is compared.
  threshold = var.ec2_asg_cpu_utilization_low_threshold_percent
  # (Optional) The list of actions to execute when this alarm transitions into an ALARM state from any other state.
  # Each action is specified as an Amazon Resource Name (ARN).
  alarm_actions = [join("", aws_autoscaling_policy.scale_down.*.arn)]
  # (Optional) The description for the alarm.
  alarm_description = "Scale AutoScaling [${local.asg_name_prefix_wo_dash}] Group down if CPU utilization is below [${var.ec2_asg_cpu_utilization_low_threshold_percent}] for [${var.ec2_asg_cpu_utilization_low_period_seconds}] seconds."
  # (Optional) The dimensions for the alarm's associated metric. For the list of available dimensions see
  # the AWS documentation:
  #   * http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  dimensions = {
    AutoScalingGroupName = join("", aws_autoscaling_group.this.*.name)
  }
}
