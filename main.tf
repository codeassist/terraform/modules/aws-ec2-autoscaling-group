locals {
  enabled = var.ec2_asg_module_enabled ? true : false

  common_tags = merge(
    var.ec2_asg_tags,
    {
      terraform = true
    }
  )
  instance_tags = merge(
    var.ec2_asg_instance_tags,
    {
      terraform = true
    }
  )
  volume_tags = merge(
    var.ec2_asg_volume_tags,
    {
      terraform = true
    }
  )

  # if prefix ends with dash '-' leave it untouched, or attach dash at the end of string otherwise
  asg_name_prefix         = substr(var.ec2_asg_name_prefix, (length(var.ec2_asg_name_prefix) - 1), 1) == "-" ? var.ec2_asg_name_prefix : format("%s-", var.ec2_asg_name_prefix)
  asg_name_prefix_wo_dash = substr(local.asg_name_prefix, 0, length(local.asg_name_prefix) - 1)

  autoscaling_enabled = (local.enabled && var.ec2_asg_autoscaling_policies_enabled) ? true : false
}


# --------------------------------------------------------------------------------------
# First create the Launch template that will be used in AutoScaling Group configuration
# --------------------------------------------------------------------------------------
resource "aws_launch_template" "this" {
  # Provides an EC2 launch template resource. Can be used to create instances or auto scaling groups. See:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html
  count = local.enabled ? 1 : 0
  depends_on = [
    var.ec2_asg_module_depends_on
  ]

  # Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = local.asg_name_prefix
  # Description of the launch template.
  description = "The EC2 Launch Template for instances in [${local.asg_name_prefix_wo_dash}] AutoScaling Group."
  # Whether to update Default Version each update.
  update_default_version = true

  # Specify volumes to attach to the instance besides the volumes specified by the AMI.
  dynamic "block_device_mappings" {
    for_each = var.ec2_asg_block_device_mappings
    content {
      # The name of the device to mount.
      device_name = lookup(block_device_mappings.value, "device_name", null)
      # Suppresses the specified device included in the AMI's block device mapping.
      no_device = lookup(block_device_mappings.value, "no_device", null)
      # The Instance Store Device Name (e.g. "ephemeral0"). See:
      #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html#InstanceStoreDeviceNames
      virtual_name = lookup(block_device_mappings.value, "virtual_name", null)

      # Configure EBS volume properties.
      dynamic "ebs" {
        for_each = flatten(list(lookup(block_device_mappings.value, "ebs", [])))
        content {
          # Whether the volume should be destroyed on instance termination
          delete_on_termination = lookup(ebs.value, "delete_on_termination", true)
          #  Enables EBS encryption on the volume. Cannot be used with `snapshot_id`.
          encrypted = lookup(ebs.value, "encrypted", null)
          # The amount of provisioned IOPS. This must be set with a `volume_type` of "io1".
          iops = lookup(ebs.value, "iops", null)
          # AWS Key Management Service (AWS KMS) customer master key (CMK) to use when creating the encrypted volume.
          # `encrypted` must be set to "true" when this is set.
          kms_key_id = lookup(ebs.value, "kms_key_id", null)
          # The Snapshot ID to mount.
          snapshot_id = lookup(ebs.value, "snapshot_id", null)
          # The size of the volume in gigabytes.
          volume_size = lookup(ebs.value, "volume_size", null)
          # The type of volume. Can be "standard", "gp2", or "io1".
          volume_type = lookup(ebs.value, "volume_type", "standard")
        }
      }
    }
  }

  # Customize the credit specification of the instance.
  dynamic "credit_specification" {
    for_each = var.ec2_asg_credit_specification != null ? [var.ec2_asg_credit_specification] : []
    content {
      # The credit option for CPU usage. Can be "standard" or "unlimited".
      # T3 instances are launched as "unlimited" by default.
      # T2 instances are launched as "standard" by default.
      cpu_credits = lookup(credit_specification.value, "cpu_credits", null)
    }
  }

  # If `true`, enables EC2 Instance Termination Protection.
  disable_api_termination = var.ec2_asg_disable_api_termination
  # If true, the launched EC2 instance will be EBS-optimized
  ebs_optimized = var.ec2_asg_ebs_optimized

  # The elastic GPU to attach to the instance.
  dynamic "elastic_gpu_specifications" {
    for_each = var.ec2_asg_elastic_gpu_specifications != null ? [var.ec2_asg_elastic_gpu_specifications] : []
    content {
      # The Elastic GPU Type. See more details at:
      #   * https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/elastic-gpus.html#elastic-gpus-basics
      type = lookup(elastic_gpu_specifications.value, "type", null)
    }
  }

  # The AMI from which to launch the instance.
  image_id = var.ec2_asg_image_id

  # The IAM Instance Profile to launch the instance with. See:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#instance-profile
  iam_instance_profile {
    # The name of the instance profile.
    name = var.ec2_asg_iam_instance_profile_name
  }

  # Shutdown behavior for the instance.
  instance_initiated_shutdown_behavior = var.ec2_asg_instance_initiated_shutdown_behavior

  # The market (purchasing) option for the instances.
  dynamic "instance_market_options" {
    for_each = var.ec2_asg_instance_market_options != null ? [var.ec2_asg_instance_market_options] : []
    content {
      # The market type. Can be spot.
      market_type = lookup(instance_market_options.value, "market_type", null)

      # The options for Spot Instance
      dynamic "spot_options" {
        for_each = flatten(list(lookup(instance_market_options.value, "spot_options", [])))
        content {
          # The required duration in minutes. This value must be a multiple of 60.
          block_duration_minutes = lookup(spot_options.value, "block_duration_minutes", null)
          # The behavior when a Spot Instance is interrupted. Can be "hibernate", "stop", or "terminate".
          instance_interruption_behavior = lookup(spot_options.value, "instance_interruption_behavior", "terminate")
          # The maximum hourly price you're willing to pay for the Spot Instances.
          max_price = lookup(spot_options.value, "max_price", null)
          # The Spot Instance request type. Can be "one-time", or "persistent".
          spot_instance_type = lookup(spot_options.value, "spot_instance_type", null)
          # The end date of the request.
          valid_until = lookup(spot_options.value, "valid_until", null)
        }
      }
    }
  }

  # The type of the instance.
  instance_type = var.ec2_asg_instance_type
  # The SSH key name that should be used for the instance.
  key_name = var.ec2_asg_key_name

  # The monitoring option for the instance. See:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#monitoring-1
  monitoring {
    # If "true", the launched EC2 instance will have detailed monitoring enabled.
    enabled = var.ec2_asg_enable_detailed_monitoring
  }

  #  Customize network interfaces to be attached at instance boot time - attaches one or more Network Interfaces
  # to the instance. See:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-eni.html
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#network-interfaces
  # Check limitations for autoscaling group at:
  #   * https://docs.aws.amazon.com/autoscaling/ec2/userguide/create-asg-launch-template.html#limitations
  # NOTE(!): https://github.com/terraform-providers/terraform-provider-aws/issues/4570
  network_interfaces {
    # Description of the network interface.
    description = format(
      "ENI for EC2 instance created by Launch Template in [%s] AutoScaling Group.",
      local.asg_name_prefix_wo_dash,
    )
    # The integer index of the network interface attachment.
    device_index = 0
    # Associate a public ip address with the network interface. Boolean value.
    associate_public_ip_address = var.ec2_asg_associate_public_ip_address
    # Whether the network interface should be destroyed on instance termination.
    delete_on_termination = true
    # A list of security group IDs to associate.
    security_groups = var.ec2_asg_security_group_ids
  }

  # The placement of the instance.
  dynamic "placement" {
    for_each = var.ec2_asg_placement != null ? [var.ec2_asg_placement] : []
    content {
      # The affinity setting for an instance on a Dedicated Host.
      affinity = lookup(placement.value, "affinity", null)
      # The Availability Zone for the instance.
      availability_zone = lookup(placement.value, "availability_zone", null)
      # The name of the placement group for the instance.
      group_name = lookup(placement.value, "group_name", null)
      # The ID of the Dedicated Host for the instance.
      host_id = lookup(placement.value, "host_id", null)
      # Reserved for future use.
      spread_domain = lookup(placement.value, "spread_domain", null)
      # The tenancy of the instance (if the instance is running in a VPC). Can be "default", "dedicated", or "host".
      tenancy = lookup(placement.value, "tenancy", null)
    }
  }

  # MUST be disabled due to limitation since `network_interfaces` block is present!
  # A list of security group IDs to associate with.
  #vpc_security_group_ids = ...

  # The tags to apply to the resources during launch. You can tag instances and volumes. More information can be
  # found in the EC2 API documentation at:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_LaunchTemplateTagSpecificationRequest.html
  tag_specifications {
    # The type of resource to tag. Valid values are `instance` and `volume`.
    resource_type = "volume"
    # A mapping of tags to assign to the resource.
    tags = merge(
      local.volume_tags,
      {
        Name = format("%s-ebs-volume", local.asg_name_prefix_wo_dash),
      },
    )
  }
  tag_specifications {
    # The type of resource to tag. Valid values are `instance` and `volume`.
    resource_type = "instance"
    # A mapping of tags to assign to the resource.
    tags = merge(
      local.instance_tags,
      {
        Name = format("%s-ec2-instance", local.asg_name_prefix_wo_dash),
      },
    )
  }

  # A mapping of tags to assign to the launch template.
  tags = merge(
    local.common_tags,
    {
      Name = format("lt-%s", local.asg_name_prefix_wo_dash)
    },
  )

  # The Base64-encoded user data to provide when launching the instance.
  user_data = var.ec2_asg_user_data

  lifecycle {
    create_before_destroy = true
  }
}

# ------------------
# AutoScaling Group
# ------------------
resource "aws_autoscaling_group" "this" {
  # Provides an AutoScaling Group resource.
  # NOTE(!): you must specify either launch_configuration, launch_template, or mixed_instances_policy.
  count = local.enabled ? 1 : 0

  depends_on = [
    var.ec2_asg_module_depends_on
  ]

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = var.ec2_asg_force_replacement ? "${local.asg_name_prefix_wo_dash}-${join("", aws_launch_template.this.*.latest_version)}-" : local.asg_name_prefix

  # (Required) The maximum size of the auto scale group.
  max_size = var.ec2_asg_max_size
  # (Required) The minimum size of the auto scale group.
  min_size = var.ec2_asg_min_size

  # (Optional) The amount of time, in seconds, after a scaling activity completes before another scaling activity can start.
  default_cooldown = var.ec2_asg_default_cooldown

  # (Optional) Launch template specification to use to launch instances.
  # NOTE(!): Either `id` or `name` must be specified.
  launch_template {
    # The ID of the launch template. Conflicts with `name`.
    id = join("", aws_launch_template.this.*.id)
    # Template version. Can be version number, "$Latest", or "$Default".
    version = "$Latest"
    # (var.2) version = aws_launch_template.this[0].latest_version
  }

  # (Optional) Time (in seconds) after instance comes into service before checking health.
  health_check_grace_period = var.ec2_asg_health_check_grace_period
  # (Optional) "EC2" or "ELB". Controls how health checking is done.
  health_check_type = var.ec2_asg_health_check_type

  # NOTE(!!!): You may want to omit desired_capacity attribute from attached aws_autoscaling_group when using
  # autoscaling policies. It's good practice to pick either manual or dynamic (policy-based) scaling.
  #desired_capacity =

  # Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force
  # an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains
  # all the instances before deleting the group. This bypasses that behavior and potentially leaves resources
  # dangling.
  force_delete = var.ec2_asg_force_delete

  # (Optional) A list of elastic load balancer names to add to the autoscaling group names. Only valid for classic load
  # balancers. For ALBs, use `target_group_arns` instead.
  load_balancers = var.ec2_asg_load_balancers
  # (Optional) A list of `aws_alb_target_group` ARNs, for use with Application or Network Load Balancing.
  target_group_arns = var.ec2_asg_target_group_arns

  # (Optional) A list of subnet IDs to launch resources in.
  vpc_zone_identifier = var.ec2_asg_subnet_ids

  # (Optional) A list of policies to decide how the instances in the auto scale group should be terminated.
  # The allowed values are:
  #   * OldestInstance,
  #   * NewestInstance,
  #   * OldestLaunchConfiguration,
  #   * ClosestToNextInstanceHour,
  #   * OldestLaunchTemplate,
  #   * AllocationStrategy,
  #   * Default.
  termination_policies = var.ec2_asg_termination_policies

  # (Optional) A list of processes to suspend for the AutoScaling Group. The allowed values are:
  #   * Launch,
  #   * Terminate,
  #   * HealthCheck,
  #   * ReplaceUnhealthy,
  #   * AZRebalance,
  #   * AlarmNotification,
  #   * ScheduledActions,
  #   * AddToLoadBalancer.
  # Note that if you suspend either the Launch or Terminate process types, it can prevent your autoscaling group
  # from functioning properly.
  suspended_processes = var.ec2_asg_suspended_processes

  # (Optional) The `tags` attributes accepts a list of maps containing the field names as keys and their respective
  # values.
  # NOTE(!): `tag` and `tags` are mutually exclusive, only one of them can be specified.
  tags = flatten([
    for key_name in keys(local.common_tags) : {
      key                 = key_name
      value               = local.common_tags[key_name]
      propagate_at_launch = true
    }
  ])

  # (Optional) The name of the placement group into which you'll launch your instances, if any.
  placement_group = var.ec2_asg_placement_group

  # (Optional) The granularity to associate with the metrics to collect. The only valid value is 1Minute.
  metrics_granularity = "1Minute"
  # (Optional) A list of metrics to collect. The allowed values are:
  #   * GroupMinSize,
  #   * GroupMaxSize,
  #   * GroupDesiredCapacity,
  #   * GroupInServiceInstances,
  #   * GroupPendingInstances,
  #   * GroupStandbyInstances,
  #   * GroupTerminatingInstances,
  #   * GroupTotalInstances.
  enabled_metrics = var.ec2_asg_enabled_metrics

  # (Optional) A maximum duration that Terraform should wait for ASG instances to be healthy before timing out.
  # Setting this to "0" causes Terraform to skip all Capacity Waiting behavior.
  wait_for_capacity_timeout = var.ec2_asg_wait_for_capacity_timeout

  # (Optional) Setting this causes Terraform to wait for this number of instances from this autoscaling group
  # to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes.
  min_elb_capacity = var.ec2_asg_min_elb_capacity
  # (Optional) Setting this will cause Terraform to wait for exactly this number of healthy instances from
  # this autoscaling group in all attached load balancers on both create and update operations. (Takes precedence
  # over `min_elb_capacity` behavior).
  wait_for_elb_capacity = var.ec2_asg_wait_for_elb_capacity

  # Allows setting instance protection. The autoscaling group will not select instances with this setting for
  # terminination during scale in events.
  protect_from_scale_in = var.ec2_asg_protect_from_scale_in

  # The ARN of the service-linked role that the ASG will use to call other AWS services.
  service_linked_role_arn = var.ec2_asg_service_linked_role_arn

  lifecycle {
    create_before_destroy = true
  }

  # Timeouts block allows you to customize how long certain operations are allowed to take before being considered
  # to have failed.
  timeouts {
    # String representation of a duration for certain operation.
    delete = "15m"
  }
}
